<?php

declare(strict_types=1);

namespace Drupal\advent_calendar\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;

/**
 * Advent Calendar style plugin.
 *
 * @ViewsStyle(
 *   id = "advent_calendar_advent_calendar",
 *   title = @Translation("Advent Calendar"),
 *   help = @Translation("@todo Add help text here."),
 *   theme = "views_style_advent_calendar_advent_calendar",
 *   display_types = {"normal"},
 * )
 */
final class AdventCalendar extends StylePluginBase {

  /**
   * {@inheritdoc}
   */
  protected $usesRowPlugin = TRUE;

  /**
   * {@inheritdoc}
   */
  protected $usesRowClass = TRUE;

  /**
   * {@inheritdoc}
   */
  protected function defineOptions(): array {
    $options = parent::defineOptions();
    $options['wrapper_class'] = ['default' => 'item-list'];
    $options['door_closed_image'] = ['default' => ''];
    $options['door_open_image'] = ['default' => ''];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state): void {
    parent::buildOptionsForm($form, $form_state);
    $form['wrapper_class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Wrapper class'),
      '#description' => $this->t('The class to provide on the wrapper, outside rows.'),
      '#default_value' => $this->options['wrapper_class'],
    ];
    $form['door_closed_image'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Door closed image path'),
      '#description' => $this->t('Path to the image file for the closed door.'),
      '#default_value' => $this->options['door_closed_image'],
    ];
    $form['door_open_image'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Door open image path'),
      '#description' => $this->t('Path to the image file for the open door.'),
      '#default_value' => $this->options['door_open_image'],
    ];
  }

}

## INTRODUCTION

Easily add an Advent Calendar to your site. Provides a View Style for formatting
View results using a Single Directory Component for formatting each door. Use
the Advent Calendar Quickstart sub-module to set up everything you need for your
calendar.

Features

- Format view results as an advent calendar.
- Format advent calendar doors using the advent_calendar:door component.
- By default, all doors will be closed, and will open as user clicks on them.
- Unpublished doors will show the closed door, but will not be clickable.
- For anonymous users, open status of calendar doors will be stored locally
  using JavaScript.
- For logged in users, open state is stored in user.data, and sent to browser as
  a cookie.

The Advent Calendar Quickstart module sets up everything needed for a calendar.

- Creates the Advent Calendar Door content type (if not present).
- Creates the Year taxonomy vocabulary.
- Adds a taxonomy term for the current year.
- Adds a calendar door node for each day of the calendar.
- Creates a view to display an advent calendar for the current year.
- Once installed, Quickstart module serves no further purpose, and may be safely
  uninstalled. It may be reinstalled the following year to create new calendar
  entries for that year.

Note that calendar doors are created as "unpublished", so you will need to be
published before they may be "opened".

## REQUIREMENTS

This module requires the Single Directory Components core module. As a result,
it's minimum required Drupal version is 10.1.

You may want to use a scheduling module such as Scheduler or Scheduled Publish
with Advent Calendar, but this is not required.

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION

If using the Quickstart module, you can simply edit the created nodes and
replace with content specific to your calendar. You will need to either manually
publish the node each day or use a scheduling module to publish manually.

If using Advent Calendar without Quickstart, you'll need to create a view with a
View Style of Advent Calendar. You will need to create hidden view fields
selecting the values required for the component properties, then add the
component import to the rewrite of the last field.

## MAINTAINERS

Current maintainers for Drupal 10:

- James Shields (lostcarpark) - https://www.drupal.org/u/lostcarpark

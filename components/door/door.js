((Drupal, once) => {
  Drupal.behaviors.door = {
    attach(context) {
      once('component--door', '.door-link', context).forEach((link) => {
        // If we are on the page, the door should be open.
        if (link.pathname === window.location.pathname) {
          localStorage.setItem(
            `advent_calendar_${link.dataset.calendar}_${link.dataset.day}`,
            'open',
          );
        }
        // If the storage item has been set, add the open class to the door.
        if (
          localStorage.getItem(
            `advent_calendar_${link.dataset.calendar}_${link.dataset.day}`,
          ) === 'open'
        ) {
          context
            .querySelector(
              `#advent-calendar-${link.dataset.calendar}-${link.dataset.day}`,
            )
            .classList.add('open');
        }
        // Add event listener to set the door open when clicked.
        link.addEventListener('click', () => {
          localStorage.setItem(
            `advent_calendar_${link.dataset.calendar}_${link.dataset.day}`,
            'open',
          );
        });
      });
    },
  };
})(Drupal, once);

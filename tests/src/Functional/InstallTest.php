<?php

declare(strict_types=1);

namespace Drupal\Tests\advent_calendar\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test description.
 *
 * @group advent_calendar_quickstart
 */
final class InstallTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'advent_calendar_quickstart',
    'field_ui',
  ];

  /**
   * Test callback.
   */
  public function testContentType(): void {
    $admin_user = $this->drupalCreateUser(['administer content types', 'administer node fields']);
    $this->drupalLogin($admin_user);
    $this->drupalGet('admin/structure/types');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementExists('xpath', '//h1[text() = "Content types"]');
    $this->assertSession()->pageTextContains('Advent Calendar Door');
    $this->drupalGet('admin/structure/types/manage/advent_calendar_door');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementExists('xpath', '//h1[em = "Advent Calendar Door"]');
    $this->drupalGet('admin/structure/types/manage/advent_calendar_door/fields');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementExists('xpath', '//h1[text() = "Manage fields"]');
    $this->assertSession()->pageTextContains('field_day');
    $this->assertSession()->pageTextContains('field_door_image');
    $this->assertSession()->pageTextContains('field_position');
    $this->assertSession()->pageTextContains('field_tags');
    $this->assertSession()->pageTextContains('field_year');
  }

}
